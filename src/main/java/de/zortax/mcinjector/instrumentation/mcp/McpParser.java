package de.zortax.mcinjector.instrumentation.mcp;// Created by leo on 31.05.18

import de.zortax.mcinjector.instrumentation.AgentConfig;
import de.zortax.mcinjector.instrumentation.McAgent;
import de.zortax.mcinjector.util.Util;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Optional;
import java.util.Scanner;

public class McpParser {

    private File fieldsFile;
    private File methodsFile;
    private File paramsFile;
    private File exceptorFile;
    private File joinedExcFile;
    private File joinedSrgFile;
    private File staticMethodsFile;

    public McpParser() {
        AgentConfig c = McAgent.getConfig();
        String currentMappings = c.baseDir + c.mappingsPath + c.release + "-" + c.version + "/";
        this.fieldsFile = new File(currentMappings + "mcp/fields.csv");
        this.methodsFile = new File(currentMappings + "mcp/methods.csv");
        this.paramsFile = new File(currentMappings + "mcp/params.csv");
        this.exceptorFile = new File(currentMappings + "srg/exceptor.json");
        this.joinedExcFile = new File(currentMappings + "srg/joined.exc");
        this.joinedSrgFile = new File(currentMappings + "srg/joined.srg");
        this.staticMethodsFile = new File(currentMappings + "srg/static_methods.txt");

        if (!doFilesExist()) {
            McAgent.warn("Couldn't find all needed files. Please delete \"" + currentMappings + "\" and restart the agent with a valid release and version!");
            throw new IllegalStateException("Couldn't find all mapping files.");
        }

    }

    public McpManager parse() {
        if (!doFilesExist())
            throw new IllegalStateException("Couldn't find all mapping files.");

        try {

            McAgent.info("Parsing MCP files...");

            McpManager mcpManager = new McpManager();

            Scanner joinedSrgScanner = new Scanner(joinedSrgFile);

            final int[] parsed = new int[3];
            long startTime = System.currentTimeMillis();

            while (joinedSrgScanner.hasNextLine()) {
                String[] currentLine = joinedSrgScanner.nextLine().split(" ");

                if (currentLine[0].equals("CL:")) {
                    if (!currentLine[1].contains("$")) {
                        mcpManager.addWrappedClass(new WrappedClass(currentLine[1], currentLine[2]));
                        parsed[0]++;
                    } else {
                        Optional<WrappedClass> parentClass = mcpManager.getWrappedClassByObfName(currentLine[1].split("$")[0]);
                        if (parentClass.isPresent()) {
                            String innerClassName = Util.lastPart(currentLine[2], "/");
                            WrappedClass innerClass;
                            if (Util.startsWithNumber(innerClassName.split("$")[1]))
                                innerClass = new WrappedClass(currentLine[1], innerClassName);
                            else
                                innerClass = new WrappedClass(currentLine[1], innerClassName.split("$")[1]);
                            innerClass.setInnerClass(true, parentClass.get());
                            parentClass.get().addInnerClass(innerClass);
                            parsed[0]++;
                        }
                    }
                } else if (currentLine[0].equals("FD:")) {
                    String[] name = currentLine[1].split("/");
                    if (!name[0].contains("$")) {
                        mcpManager.getWrappedClassByObfName(name[0]).ifPresent(wc -> {
                            wc.addField(new WrappedField(name[1], Util.lastPart(currentLine[2], "/")));
                            parsed[1]++;
                        });
                    } else {
                        mcpManager.getWrappedClassByObfName(name[0].split("$")[0]).ifPresent(wc -> {
                            wc.getInnerClasses()
                                    .stream()
                                    .filter(i -> i.getObfuscatedName().equals(name[0]) || i.getObfuscatedName().equals(name[0].split("$")[1]))
                                    .findFirst().ifPresent(innerClass -> {
                                        innerClass.addField(new WrappedField(name[1], Util.lastPart(currentLine[2], "/")));
                                        parsed[1]++;
                                    }
                            );
                        });
                    }
                }

            }

            McAgent.info("Parsed " + parsed[0] + " classes, " + parsed[1] + " fields and " + parsed[2] + " functions in " + (System.currentTimeMillis() - startTime) + " ms.");

            return mcpManager;

        } catch (FileNotFoundException e) {
            if (McAgent.getConfig().log)
                e.printStackTrace();
        }

        return null;

    }

    public boolean doFilesExist() {
        return fieldsFile.exists() && fieldsFile.isFile()
                && methodsFile.exists() && methodsFile.isFile()
                && paramsFile.exists() && paramsFile.isFile()
                && exceptorFile.exists() && exceptorFile.isFile()
                && joinedExcFile.exists() && joinedExcFile.isFile()
                && joinedSrgFile.exists() && joinedSrgFile.isFile()
                && staticMethodsFile.exists() && staticMethodsFile.isFile();
    }

}
