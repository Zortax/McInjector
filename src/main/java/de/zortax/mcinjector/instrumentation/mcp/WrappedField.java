package de.zortax.mcinjector.instrumentation.mcp;// Created by leo on 13.06.18

public class WrappedField {

    private String obfuscatedName;
    private String srgName;
    private boolean isStatic;

    public WrappedField(String obfuscatedName, String srgName) {
        this.obfuscatedName = obfuscatedName;
        this.srgName = srgName;
    }

    public String getObfuscatedName() {
        return obfuscatedName;
    }

    public String getSrgName() {
        return srgName;
    }

    public void setStatic(boolean isStatic) {
        this.isStatic = isStatic;
    }

    public boolean isStatic() {
        return isStatic;
    }

}
