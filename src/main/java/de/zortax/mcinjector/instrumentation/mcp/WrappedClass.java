package de.zortax.mcinjector.instrumentation.mcp;// Created by leo on 01.06.18

import de.zortax.mcinjector.util.Util;

import java.util.ArrayList;
import java.util.Optional;

public class WrappedClass {

    private String obfuscatedName;
    private String mcpName;
    private String mcpSimpleName;
    private ClassType type = null;

    private ArrayList<WrappedFunction> functions;
    private ArrayList<WrappedField> fields;

    private ArrayList<WrappedClass> innerClasses;

    private Class classObject = null;

    private boolean innerClass;
    private WrappedClass innerClassOwner;

    public WrappedClass(String obfuscatedName, String mcpName) {
        this(obfuscatedName, mcpName, false);
    }

    public WrappedClass(String obfuscatedName, String mcpName, boolean innerClass) {
        this.obfuscatedName = obfuscatedName;
        this.mcpName = mcpName.replaceAll("/", ".");
        this.mcpSimpleName = Util.lastPart(mcpName, "/");
        this.functions = new ArrayList<>();
        this.fields = new ArrayList<>();
        this.innerClass = innerClass;
        this.innerClasses = new ArrayList<>();
        this.innerClassOwner = null;
    }

    public void setClassObject(Class clazz) {
        this.classObject = clazz;

        if (clazz.isInterface())
            type = ClassType.INTERFACE;
        else if (clazz.isEnum())
            type = ClassType.ENUM;
        else if (clazz.isAnnotation())
            type = ClassType.ANNOTATION;
        else
            type = ClassType.CLASS;

    }

    public String getObfuscatedName() {
        return obfuscatedName;
    }

    public String getMcpName() {
        return mcpName;
    }

    public String getMcpSimpleName() {
        return mcpSimpleName;
    }

    public ClassType getType() {
        if (type == null)
            throw new IllegalStateException("Class type not loaded yet.");
        return type;
    }

    public Class getClassObject() {
        if (classObject == null)
            throw new IllegalStateException("Class object not loaded yet.");
        return classObject;
    }

    public ArrayList<WrappedFunction> getFunctions() {
        return functions;
    }

    public ArrayList<WrappedField> getFields() {
        return fields;
    }

    public Optional<WrappedFunction> getFunctionBySrgName(String srgName) {
        return functions.stream().filter(f -> f.getSrgName().equals(srgName)).findFirst();
    }

    public Optional<WrappedFunction> getFunctionBySrgName(String srgName, String srgNameDescriptor) {
        return functions.stream().filter(f -> f.getSrgName().equals(srgName) && f.getSrgDescriptor().equals(srgNameDescriptor)).findFirst();
    }

    public Optional<WrappedFunction> getFunctionByMcpName(String mcpName) {
        return functions.stream().filter(f -> f.getMcpName().equals(mcpName)).findFirst();
    }

    public Optional<WrappedFunction> getFunctionByMcpName(String mcpName, String srgNameDescriptor) {
        return functions.stream().filter(f -> f.getMcpName().equals(mcpName) && f.getSrgDescriptor().equals(srgNameDescriptor)).findFirst();
    }

    public Optional<WrappedField> getFieldBySrgName(String srgName) {
        return fields.stream().filter(f -> f.getSrgName().equals(srgName)).findFirst();
    }

    public Optional<WrappedField> getFieldByObfName(String obfName) {
        return fields.stream().filter(f -> f.getObfuscatedName().equals(obfName)).findFirst();
    }

    public void addField(WrappedField field) {
        fields.add(field);
    }

    public void addFunction(WrappedFunction function) {
        functions.add(function);
    }

    public boolean isInnerClass() {
        return innerClass;
    }

    public WrappedClass getInnerClassOwner() {
        if (!innerClass)
            throw new IllegalStateException("This is not an inner class.");
        if (innerClassOwner == null)
            throw new IllegalStateException("Owner of this inner class not loaded.");
        return innerClassOwner;
    }

    public void addInnerClass(WrappedClass innerClass) {
        this.innerClasses.add(innerClass);
    }

    public void setInnerClass(boolean innerClass, WrappedClass owner) {
        if (owner != null)
            this.innerClassOwner = owner;
        this.innerClass = innerClass;
    }

    public void setInnerClass(boolean innerClass) {
        setInnerClass(innerClass, null);
    }

    public ArrayList<WrappedClass> getInnerClasses() {
        return innerClasses;
    }

}
