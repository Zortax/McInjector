package de.zortax.mcinjector.instrumentation.mcp;// Created by leo on 13.06.18

import java.util.ArrayList;
import java.util.Optional;

public class McpManager {

    private ArrayList<WrappedClass> wrappedClasses;

    protected McpManager() {
        this.wrappedClasses = new ArrayList<>();
    }

    public ArrayList<WrappedClass> getWrappedClasses() {
        return wrappedClasses;
    }

    public void addWrappedClass(WrappedClass wrappedClass) {
        wrappedClasses.add(wrappedClass);
    }

    public Optional<WrappedClass> getWrappedClassByMcpSimpleName(String simpleName) {
        return wrappedClasses.stream().filter(wc -> wc.getMcpSimpleName().equals(simpleName)).findFirst();
    }

    public Optional<WrappedClass> getWrappedClassByMcpName(String mcpName) {
        return wrappedClasses.stream().filter(wc -> wc.getMcpName().equals(mcpName)).findFirst();
    }

    public Optional<WrappedClass> getWrappedClassByObfName(String obfName) {
        return wrappedClasses.stream().filter(wc -> wc.getObfuscatedName().equals(obfName)).findFirst();
    }

}
